# Overview of Bayesian Optimisation Acquisition Functions for Hyperparameter Optimisation

**Authors**: Mariya Mladenova, Petar Tonchev, Osman Deger

In this project we introduce the concepts of Bayesian Hyperparameter Optimisation and investigate its behaviour on some simple objective functions.
We implemented a Bayesian optimisation framework and investigated three popular choices for acquisition functions- PI, EI and Thompson Sampling.

For more details view the [report of the project](https://gitlab.com/p.tonchev96/bayesian-hyperparameter-optimisation/-/raw/master/Bayesian_Hyperparameter_Optimisation.pdf)